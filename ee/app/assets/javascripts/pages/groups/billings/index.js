import initSubscriptions from 'ee/billings';

document.addEventListener('DOMContentLoaded', () => {
  initSubscriptions();
});
